

/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("arrowright").style.visibility = "hidden";
    if(window.screen.width>1024){
    document.getElementById("main").style.marginLeft = "250px";
    }else if(window.screen.width<1024){
      document.getElementById("main").style.marginLeft = "0px";
    }
  }
  
  /* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    document.getElementById("arrowright").style.visibility = "visible";
  }

// For printing purposes
function myFunction() {
    window.print()
}